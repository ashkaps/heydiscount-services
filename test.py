from logging import log
import os
import logging 
from flask import Flask, render_template, request, json, jsonify, make_response
try:
    from PIL import Image
except ImportError:
    import Image
# import pytesseract
import os
import mongo as mongo
from flask_cors import CORS, cross_origin


from google.cloud import vision
import io

#Create and configure logger 
logging.basicConfig(filename="newfile.log", 
                    format='%(asctime)s %(message)s', 
                    filemode='w') 
  #Creating an object 
logger=logging.getLogger() 
  
#Setting the threshold of logger to DEBUG 
logger.setLevel(logging.DEBUG) 

# define a folder to store and later serve the images
UPLOAD_FOLDER = '/static/uploads/'

# allow files of a specific type
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# function to check the file extension
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/health', methods=['GET'])
def test():
    return 'ok'

@app.route('/upload', methods=['GET'])
def upload():
    mongo.add_to_mongo()

# route and function to handle the upload page
@app.route('/upload1', methods=['POST'])
def upload_page():
    try:
        if request.method == 'POST':
            # check if there is a file in the request
            if 'file' not in request.files:
                return jsonify(text='upload a file. file missing ',status=500,mimetype='application/json')
            file = request.files['file']
            # if no file is selected
            if file.filename == '':
                # return render_template('upload.html', msg='No file selected')
                return jsonify(text='upload a file. no filename ',status=500,mimetype='application/json')

            if file and allowed_file(file.filename):

                
                logger.info('file received: ' + file.filename)
                extracted_text = ocr_core_vision(file)
                # extracted_text = ocr_core(file)
                print(extracted_text)
                
                # resp = jsonify(text=extracted_text,status=200,mimetype='application/json')
                return extracted_text
                
                return resp
            
            return jsonify(text='server error',status=500,mimetype='application/json')
        return jsonify(text='failed condition',status=500,mimetype='application/json')
    
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')


# route and function to handle the upload page
@app.route('/getstores', methods=['POST'])
def retrieve_stores_for_user():
    try:
        req_data = request.get_json()
        if req_data is None:
            return jsonify(error='no request data. send user.',status=400,mimetype='application/json')
        else:
            user = req_data['user']
            if user is None:
                return jsonify(error='user value not sent in request',status=400,mimetype='application/json')
            else:
                store_list, notes = mongo.get_stores_from_mongo(user)
                response = jsonify(store_list=store_list, notes = notes, status=200,mimetype='application/json')
                # r = make_response(store_list=store_list, notes = notes, status=200,mimetype='application/json')
                
                response.headers["Cache-Control"] = 'max-age=7200'
                
                
                return response
    
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')


# route and function to handle the upload page
@app.route('/getstores/<user>', methods=['GET'])
def get_stores_for_user(user):
    try:
        store_list, notes = mongo.get_stores_from_mongo(user)
        response = jsonify(store_list=store_list, notes = notes, status=200,mimetype='application/json')
        response.headers["Cache-Control"] = 'max-age=7200'
        return response
    
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')



@app.route('/nocouponfound', methods=['POST'])
def no_coupons_on_site():
    try:
        req_data = request.get_json()
        if req_data is None:
            return jsonify(error='no request data. send user.',status=400,mimetype='application/json')
        else:
            user = req_data['user']
            storename = req_data['storename']
            if user is None:
                return jsonify(error='user value not sent in request',status=400,mimetype='application/json')
            else:
                mongo.no_coupon_on_store(user, storename)
                return jsonify(message = 'Saved!', status=200,mimetype='application/json')
    except KeyError as e:
        return jsonify(error='user or storename not sent in request',status=400,mimetype='application/json')
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')



# route and function to handle the upload page
@app.route('/savecoupon', methods=['POST'])
def save_coupon():
    try:
        req_data = request.get_json()
        if req_data is None:
            return jsonify(error='no request data. send user.',status=400,mimetype='application/json')
        else:
            # json_data = json.loads(req_data)
            user = req_data['user']
            store = req_data['store']
            code = req_data['code']
            title = req_data['title']
            expiry = req_data['expiry']
            no_coupons = False
            edited = False
            edit_coupon_id = ''
            if 'no_coupons' in req_data:
                no_coupons = True
            if 'edited' in req_data:
                edited = req_data['edited']
                edit_coupon_id = req_data['coupon_id']
            if user is None or store is None or code is None or title is None:
                return jsonify(error='user value not sent in request',status=400,mimetype='application/json')
            else:
                coupon_id = mongo.save_coupon(user, store, code, title, expiry, no_coupons, edit_coupon_id,edited)
                return jsonify(coupon_id = coupon_id, message='Data added, thanks!',status=200,mimetype='application/json')
    
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')

@app.route('/undo', methods=['POST'])
def undo():
    try:
        req_data = request.get_json()
        if req_data is None:
            return jsonify(error='no request data. send user.',status=400,mimetype='application/json')
        else:
            # json_data = json.loads(req_data)
            coupon_id = req_data['coupon_id']
           
            data = mongo.retrieve_coupon(coupon_id)
            return jsonify(data = data,status=200,mimetype='application/json')
    
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')



# route and function to handle the upload page
@app.route('/getusers', methods=['GET'])
def get_users():
    try:
        users = mongo.get_users_from_mongo()
        return jsonify(users=users,status=200,mimetype='application/json')
    
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')


@app.route('/getcoupons', methods=['GET'])
def get_coupons():
    try:
        coupons = mongo.get_coupons_from_mongo()
        return jsonify(coupons=coupons,status=200,mimetype='application/json')
    
    except Exception as e:
        logger.info('exception')
        logger.exception("failed to return. " + str(e))
        return jsonify(text='error. ' + str(e) ,status=500,mimetype='application/json')




# def ocr_core(filename):
#     """
#     Tesseract
#     This function will handle the core OCR processing of images.
#     """
#     img = Image.open(filename)
#     img.save('img.png')
#     text1 = pytesseract.image_to_string(img).strip()  # We'll use Pillow's Image class to open the image and pytesseract to detect the string in the image
#     # logger.info('simple tess: >> ' + text1)
#     # text2 = get_string('img.png',4).strip()
#     # logger.info('image pre-processing result: >>> ' + text2)
#     # return text1 + '   \n     ||||     \n     ' + text2
#     text = text1.replace('\n', ' ').replace('\r', '')
#     text = text.encode("ascii", errors="ignore").decode()
#     os.remove("img.png") 
#     return text

def ocr_core_vision(filename):
    """
    This function will handle the core OCR processing of images using Google Vision.
    """
    try:
        filepath = os.path.join(os.getcwd(), 'images')
        try:
            if os.path.isdir(filepath):
                os.remove(filepath)
                os.mkdir(filepath)
        except Exception as e:
            print(str(e))
            
        try:
            filepath = os.path.join(filepath, 'img.png')
            if os.path.isfile(filepath):
                os.remove(filepath)
        except Exception as e:
            print(str(e))
            
        img = Image.open(filename)
        img.save(filepath)
        
        client = vision.ImageAnnotatorClient()
        with io.open(filepath, 'rb') as image_file:
            content = image_file.read()
        image = vision.Image(content=content)
        response = client.text_detection(image=image)
        texts = response.text_annotations
        final_text = texts[0].description.strip()
        if response.error.message:
            raise Exception(
                '{}\nFor more info on error messages, check: '
                'https://cloud.google.com/apis/design/errors'.format(
                    response.error.message))
        final_text = final_text.replace('\r', ' ').replace('\n', ' ') 
        final_text = ' '.join(final_text.split())
        return final_text.strip()
    except Exception as e:
        print(str(e))
        return ""




if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)