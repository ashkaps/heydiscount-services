from proxy_requests import ProxyRequests
import csv
import multiprocessing
import os

cpu_count = os.cpu_count()
failed_list = list()
output = list()

def check_status_code(u):
    try :

        url = "http://" + u[0]
        print(url)
        req = ProxyRequests(url)
        req.get()
        
        out = dict()
        out['store'] = u
        out['status code'] = req.get_status_code()
        
    except Exception as e:
        failed_list.append(u)

if __name__ == '__main__':
    req = ProxyRequests('https://stackoverflow.com/questions/57198719/python-multiprocessing-with-list-input-from-csv')
    req.get()
    print(req.get_status_code())
    print(req.get_headers())
    with open('viglink.csv', newline='') as f:
        reader = csv.reader(f)
        with multiprocessing.Pool(cpu_count) as p:
            buffer = []
            for row_num, row in enumerate(reader):
                if row_num == 0:
                    continue
                buffer.append(row)
                # if read enough rows -> do work
                if row_num % cpu_count == 0:
                    p.map(check_status_code, buffer)
                    # don't forget to clear the buffer
                    buffer = []
            else:
                # process leftover rows
                p.map(check_status_code, buffer)


    keys = output[0].keys()

    with open('output.csv', 'w', newline='')  as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(output)


    with open('failed.csv', 'w', newline='')  as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(failed_list)
