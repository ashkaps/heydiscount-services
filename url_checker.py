from proxy_requests import ProxyRequests
import pandas as pd 
import csv
from multiprocessing import Pool



failed_list = list()
output = list()


def check_status_code(u):
    try :

        url = "http://" + u
        print(url)
        req = ProxyRequests(url)
        req.get()
        
        out = dict()
        out['store'] = u
        out['status code'] = req.get_status_code()
    except Exception as e:
        failed_list.append(u)



# if __name__ == '__main__':
#     rows = pd.read_csv("viglink.csv") 
#     all_urls = list()
#     pool = Pool()                         # Create a multiprocessing Pool
#     for a, r in rows.iterrows():
#         url = "http://" + r['Sites']
#         pool.apply_async(check_status_code, (url))
#     pool.close()
#     pool.join()

if __name__ == '__main__':
    rows = pd.read_csv("viglink.csv") 
    all_urls = list()
    pool = Pool()                         # Create a multiprocessing Pool
    # args = ["http://" + r['Sites'] 
    #     for a, r in rows.iterrows()]
    # for a, r in rows.iterrows():
    #     url = "http://" + r['Sites']
    #     all_urls.append(url)
    myList = [r for a, r in rows.iterrows()]   
    pool.starmap(check_status_code, all_urls)
    pool.close()
    pool.join()
    
    
    # pool.map(check_status_code, all_urls)  # proce
    keys = output[0].keys()

    with open('output.csv', 'w', newline='')  as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(output)


    with open('failed.csv', 'w', newline='')  as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(failed_list)
