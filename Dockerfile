FROM python:3.7
RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev build-essential
RUN apt update && apt install -y libsm6 libxext6
# RUN apt-get -y install tesseract-ocr
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt 
EXPOSE 5000
ENTRYPOINT [ "python3" ] 
CMD [ "main.py" ] 