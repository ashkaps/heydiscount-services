try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract
import logging 
import numpy as np
import os
import cv2



#Create and configure logger 
logging.basicConfig(filename="newfile.log", 
                    format='%(asctime)s %(message)s', 
                    filemode='w') 
  #Creating an object 
logger=logging.getLogger() 
  
#Setting the threshold of logger to DEBUG 
logger.setLevel(logging.DEBUG) 
