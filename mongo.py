import pandas as pd
import os
# from pymongo import MongoClient
import pymongo
from datetime import date, datetime
import bson
import uuid 
from datetime import date, timedelta
import csv
import time


#CSV to JSON Conversion

client = pymongo.MongoClient("mongodb+srv://crest:qyEO1jGZbasmTJZ1@cn.35i8x.mongodb.net/couponninja?retryWrites=true&w=majority")
database = client.couponninja


def find_csv_filenames( path_to_dir, suffix=".xlsx" ):
    filenames = os.listdir(path_to_dir)
    return [ filename for filename in filenames if filename.endswith( suffix ) ]


def save_coupon(user, store, code, title, expiry, start_date, no_coupons, edit_coupon_id, edited, coupon_url, terms):
    if expiry == '':
        days_after = (date.today()+timedelta(days=14)).isoformat()  
        expiry = days_after
    today = date.today()
    title = title.replace("=","").strip()
    code = code.replace("=","").strip()
    store = store.strip().lower()
    
    insert_date = today.strftime("%m/%d/%Y")
    collection = database['captured_coupons']
    coupon_id = uuid.uuid1().hex
    if edited:
        collection.delete_one({"uuid":edit_coupon_id})
    data = {'user':user, 'coupon_url':coupon_url, 'store':store,'code':code,'title':title, 'start_date':start_date, 'expiry':expiry,'uploaded':False, 'uuid':coupon_id,'timestamp':str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')),'insert_date':insert_date,'no_coupons':no_coupons, 'terms': terms}
    collection.insert_one(data)
    return coupon_id

def no_coupon_on_store(user, store):
    today = date.today()
    insert_date = today.strftime("%m/%d/%Y")
    collection = database['no_coupons']
    data = {'user':user, 'store':store,'uploaded':False, 'timestamp':str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')),'insert_date':insert_date}
    collection.insert_one(data)

def add_to_mongo():
    dir = os.getcwd()
    print(dir)
    csv_files = find_csv_filenames(dir)
    collection = database['stores']
    all_stores = list()
    all_notes = list()
    for file in csv_files:
        print('file: ' + file)
        excel = pd.ExcelFile(file, engine='openpyxl')
        sheets = excel.sheet_names
        for sht in sheets:  
            print('sheet: ' + sht)
            stores = list()
            notes = list()
            sheet_df = excel.parse(sht)
            for index, row in sheet_df.iterrows():
                store = row['Stores']
                note = row['Notes']
                if type(store) == str:
                    stores.append(store)
                    if type(note) == str and note != '' and note != 'nan':
                        a = dict()
                        a['store'] = store
                        a['notes'] = note
                        notes.append(a)
        
            new_rows = list()
            row1 = dict()
            row1.update({'user':sht})
            row1.update({'timestamp':str(date.today())})
            row1.update({'store':stores})
            row1.update({'notes':notes})
            new_rows.append(row1)
            # collection.insert_one(row1)
            all_notes.extend(notes)
            all_stores.extend(stores)


        row1 = dict()
        row1.update({'user':'ADMIN'})
        row1.update({'timestamp':str(date.today())})
        row1.update({'store':all_stores})
        row1.update({'notes':all_notes})
        new_rows.append(row1)
        collection.insert_one(row1)



def add_to_mongo_old():
    # dir = os.getcwd() + '/Manual Coupons'
    dir = os.getcwd()
    print(dir)
    csv_files = find_csv_filenames(dir)
    collection = database['stores']
    all_stores = list()
    all_notes = list()
    for file in csv_files:
        print('file: ' + file)
        
        stores, notes = csv_to_json(dir,file)
        
        new_rows = list()
        row1 = dict()
        row1.update({'user':file.replace(".xlsx","")})
        row1.update({'timestamp':str(date.today())})
        row1.update({'store':stores})
        row1.update({'notes':notes})
        new_rows.append(row1)
        # collection.insert_many(new_rows)
        collection.insert_one(row1)
        all_notes.extend(notes)
        all_stores.extend(stores)

    new_rows = list()
    row1 = dict()
    row1.update({'user':'Email Team'})
    row1.update({'timestamp':str(date.today())})
    row1.update({'store':all_stores})
    row1.update({'notes':all_notes})
    new_rows.append(row1)
    collection.insert_one(row1)


def csv_to_json(dir, filename, header=None):
    filename = dir + "/" + filename
    excel = pd.ExcelFile(filename)
    sheets = excel.sheet_names
    stores = list()
    notes = list()
    for sht in sheets:
        if 'Stores W' in sht:
            sheet_df = excel.parse(sht)
            for index, row in sheet_df.iterrows():
                store = row['Store']
                note = row['Notes']
                if type(store) == str:
                    stores.append(store)
                    if type(note) == str and note != '' and note != 'nan':
                        a = dict()
                        a['store'] = store
                        a['notes'] = note
                        notes.append(a)
    return stores, notes



def retrieve_coupon(coupon_id):
    collection = database['captured_coupons']
    cursor = collection.find({"uuid":coupon_id})
    for document in cursor:
        del document['_id'] 
        return document

def update_insert_date():
    collection = database['captured_coupons']
    cursor = collection.find({})
    for document in cursor:
        update_doc = True
        if 'insert_date' in document:

            insert_date = document['insert_date']
            if insert_date == '2020-01-04' or insert_date == '2020-01-05' or insert_date == '2020-01-06':
                update_doc = False
            if insert_date is None or insert_date == '' or '2020-01-04' in insert_date:
                insert_date = '2020-01-04'
            elif  '2020-01-05' in insert_date:
                insert_date = '2020-01-05'
            elif  '2020-01-06' in insert_date:
                insert_date = '2020-01-06'
        else:
            insert_date = '2020-01-04'
        if update_doc:
            collection.update({'_id': document['_id']}, {"$set": {'insert_date': insert_date}})

def get_stores_from_mongo(user):
    collection = database['stores']
    cursor = collection.find({"user":user})
    for document in cursor:
        store = document['store']
        note = document['notes']
        store_names = document['store_names']
        return store, note, store_names

def get_users_from_mongo():
    collection = database['stores']
    users = collection.find().distinct('user')
    return users

def get_coupons_from_mongo_user(user):
    collection = database['captured_coupons']
    coupons = collection.find({"user":user})
    rows = list()
    for document in coupons:
        del document['_id'] 
        rows.append(document)
    keys = rows[0].keys()
    with open(user + '.csv', 'w', newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(rows)
    
def login_auth(username, password):
    collection = database['users']
    user = collection.find({"username":str(username)})
    actual_password = None
    user_found = False
    name = None
    for doc in user:
        actual_password = doc['password']
        name = doc['name']
        user_found = True
        break
    if not user_found:
        return False, "Invalid username"
    if actual_password != None and actual_password == password:
        return True, name
    else: 
        return False, "Invalid password"



if __name__ == "__main__":
    get_stores_from_mongo("ADMIN")