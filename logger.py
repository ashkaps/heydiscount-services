import logging.handlers as handlers
import logging
import os
from datetime import date

log_dir = 'var/log/'
if not os.path.exists(log_dir):
    os.makedirs(log_dir)
log_file = os.path.join(log_dir, 'kaprinapi.log')
logging.basicConfig(filename=log_file, level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

file_handler =  handlers.TimedRotatingFileHandler('kaprin:'+ str(date.today())+'.log',when='midnight',interval=1,backupCount=30)
log_name = 'kaprinapi'
logging.captureWarnings(False)
logging.getLogger("requests").setLevel(logging.ERROR)
logging.getLogger("urllib3").setLevel(logging.ERROR)
logging.getLogger("urllib3").propagate = False
logging.getLogger("requests").propagate = False
logging.getLogger('webdriver_manager.logger').setLevel(logging.ERROR)
logging.getLogger('seleniumwire').setLevel(logging.CRITICAL)
console_handler = logging.StreamHandler()
logger = logging.getLogger(log_name)
logger.addHandler(console_handler)
logger.setLevel(logging.INFO)

file_format = logging.Formatter('%(asctime)s - [%(thread)d] - %(module)s - [%(levelname)s] - %(message)s')
file_handler.setFormatter(file_format)
logger.addHandler(file_handler)
